.. _other_resources:

Other Resources
===============


:ref:`hid_primer`
  A quick overview of how HID works and how we use it
:ref:`huion_k20`
  A step-by-step explanation on how to add a new Huion device.
:ref:`spacenavigator_case_study`
  Tracing the impact of a device quirk through the Linux input stack.
